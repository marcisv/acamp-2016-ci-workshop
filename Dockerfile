FROM python:alpine
WORKDIR /app

ADD ./requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

ENV GREET_LOC lv
CMD ["python","./hello.py"]
